//
//  main.m
//  socketProgramming
//
//  Created by sangjune moon on 2014. 12. 30..
//  Copyright (c) 2014년 nexmo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "nmAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([nmAppDelegate class]));
    }
}
