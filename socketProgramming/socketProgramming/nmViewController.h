//
//  nmViewController.h
//  socketProgramming
//
//  Created by sangjune moon on 2014. 12. 30..
//  Copyright (c) 2014년 nexmo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface nmViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *NMLabelMyIP;
@property (weak, nonatomic) IBOutlet UITextField *NMTextFieldServerIP;
@property (weak, nonatomic) IBOutlet UITextField *NMTextFieldServerPort;
@property (weak, nonatomic) IBOutlet UILabel *NMUILabelRecievedMsg;
@property (weak, nonatomic) IBOutlet UILabel *NMLabelServerStatus;
- (IBAction)ServerButtonTouch:(id)sender;
- (IBAction)ClientButtonTouch:(id)sender;
- (IBAction)SendMsg01Touch:(id)sender;
- (IBAction)SendMsg02Touch:(id)sender;
- (IBAction)SendMsg03Touch:(id)sender;

@end
