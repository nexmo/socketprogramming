//
//  nmViewController.m
//  socketProgramming
//
//  Created by sangjune moon on 2014. 12. 30..
//  Copyright (c) 2014년 nexmo. All rights reserved.
//

#import "nmViewController.h"

//Find my ip
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>

//Fast socket
#import "FastSocket.h"
#import "FastServerSocket.h"

//sound
#import <AudioToolbox/AudioServices.h>

//tag define
#define tag05ServerIP        5
#define tag07ServerPort      7

#define ClientTagStartIndex  21
#define ClientTagEndIndex    23

#define IOS_CELLULAR    @"pdp_ip0"
#define IOS_WIFI        @"en0"
#define IP_ADDR_IPv4    @"ipv4"
#define IP_ADDR_IPv6    @"ipv6"

#define BUFFER_SIZE     1024

#define serverDefaultPort @"8000"
#define serverDefaultIP   @"192.168.1.63"

@interface nmViewController (){

    NSString *serverIP;
    NSString *serverPort;
    
    NSString *myIP;

	int sendSocket;
	int recieveSocket;
	
	struct sockaddr_in sendAddr;
	struct sockaddr_in recieveAddr;

    //FastSocket
	FastSocket *client;
	FastServerSocket *server;

    //buffer setup
    unsigned char buf[BUFFER_SIZE];

}

@end

@implementation nmViewController

@synthesize NMTextFieldServerIP;
@synthesize NMTextFieldServerPort;
@synthesize NMUILabelRecievedMsg;
@synthesize NMLabelServerStatus;
@synthesize NMLabelMyIP;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    serverIP = serverDefaultIP;
    serverPort = serverDefaultPort;
    
    NMTextFieldServerIP.text = serverIP;
    NMTextFieldServerPort.text = serverPort;
    
    NMLabelServerStatus.text = @"NOP";
    
    NMLabelMyIP.text = [self getIPAddress:YES];
    
   [self clientEnable];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)getIPAddress:(BOOL)preferIPv4
{
    NSArray *searchArray = preferIPv4 ?
    @[ IOS_WIFI @"/" IP_ADDR_IPv4,
       IOS_WIFI @"/" IP_ADDR_IPv6,
       IOS_CELLULAR @"/" IP_ADDR_IPv4,
       IOS_CELLULAR @"/" IP_ADDR_IPv6
       ] :
    @[ IOS_WIFI @"/" IP_ADDR_IPv6,
       IOS_WIFI @"/" IP_ADDR_IPv4,
       IOS_CELLULAR @"/" IP_ADDR_IPv6,
       IOS_CELLULAR @"/" IP_ADDR_IPv4
       ] ;
    
    NSDictionary *addresses = [self getIPAddresses];
    NSLog(@"addresses: %@", addresses);
    
    __block NSString *address;
    [searchArray enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop)
     {
         address = addresses[key];
         if(address) *stop = YES;
     } ];
    return address ? address : @"0.0.0.0";
}

- (NSDictionary *)getIPAddresses
{
    NSMutableDictionary *addresses = [NSMutableDictionary dictionaryWithCapacity:8];
    
    // retrieve the current interfaces - returns 0 on success
    struct ifaddrs *interfaces;
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        struct ifaddrs *interface;
        for(interface=interfaces; interface; interface=interface->ifa_next) {
            if(!(interface->ifa_flags & IFF_UP) /* || (interface->ifa_flags & IFF_LOOPBACK) */ ) {
                continue; // deeply nested code harder to read
            }
            const struct sockaddr_in *addr = (const struct sockaddr_in*)interface->ifa_addr;
            char addrBuf[ MAX(INET_ADDRSTRLEN, INET6_ADDRSTRLEN) ];
            if(addr && (addr->sin_family==AF_INET || addr->sin_family==AF_INET6)) {
                NSString *name = [NSString stringWithUTF8String:interface->ifa_name];
                NSString *type;
                if(addr->sin_family == AF_INET) {
                    if(inet_ntop(AF_INET, &addr->sin_addr, addrBuf, INET_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv4;
                    }
                } else {
                    const struct sockaddr_in6 *addr6 = (const struct sockaddr_in6*)interface->ifa_addr;
                    if(inet_ntop(AF_INET6, &addr6->sin6_addr, addrBuf, INET6_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv6;
                    }
                }
                if(type) {
                    NSString *key = [NSString stringWithFormat:@"%@/%@", name, type];
                    addresses[key] = [NSString stringWithUTF8String:addrBuf];
                }
            }
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    return [addresses count] ? addresses : nil;
}

-(void)clientEnable
{
	[server close];
	client = [[FastSocket alloc] initWithHost:serverIP andPort:serverPort];
    
    UIView *grayView01 = [self.view viewWithTag:10];
    UIView *grayView02 = [self.view viewWithTag:24];
    
    grayView01.hidden = NO;
    grayView02.hidden = YES;
    
}

-(void)serverEnable
{
	server = [[FastServerSocket alloc] initWithPort:serverPort];
	[client close];

    UIView *grayView01 = [self.view viewWithTag:10];
    UIView *grayView02 = [self.view viewWithTag:24];
    
    grayView01.hidden = YES;
    grayView02.hidden = NO;

    
}


- (IBAction)ServerButtonTouch:(id)sender {
    
    [self serverEnable];
    
    [self testSendingAndReceivingStrings];
    
}

- (IBAction)ClientButtonTouch:(id)sender {
    
    [self clientEnable];
    
    client= [[FastSocket alloc] initWithHost:serverIP andPort:serverPort];
    
    [client connect];

}

- (IBAction)SendMsg01Touch:(id)sender {
    
	[client connect];
	
	// Send the string.
	NSString *original = @"Msg1";
	NSData *data = [original dataUsingEncoding:NSUTF8StringEncoding];
	long len = [data length];
	long count = [client sendBytes:[data bytes] count:len];
    count = count;
	NSLog(@"send error: %@:[%zd]", ([client lastError]==nil?@"NONE":[client lastError]),count);
	[[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Compare results.
	[client close];
    
}

- (IBAction)SendMsg02Touch:(id)sender {
    
	[client connect];
	
	// Send the string.
	NSString *original = @"Msg2";
	NSData *data = [original dataUsingEncoding:NSUTF8StringEncoding];
	long len = [data length];
	long count = [client sendBytes:[data bytes] count:len];
    count = count;
	NSLog(@"send error: %@:[%zd]", ([client lastError]==nil?@"NONE":[client lastError]),count);
	[[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Compare results.
	[client close];
    
}

- (IBAction)SendMsg03Touch:(id)sender {
    
	[client connect];
	
	// Send the string.
	NSString *original = @"Msg3";
	NSData *data = [original dataUsingEncoding:NSUTF8StringEncoding];
	long len = [data length];
	long count = [client sendBytes:[data bytes] count:len];
    count = count;
	NSLog(@"send error: %@:[%zd]", ([client lastError]==nil?@"NONE":[client lastError]),count);
	[[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Compare results.
	[client close];
    
}

-(void)textFieldDidEndEditing:
(UITextField*)textField
{
    if(textField.tag == tag05ServerIP){

        serverIP = NMTextFieldServerIP.text;
        
    }
    else if(textField.tag == tag07ServerPort){
        
        serverPort = NMTextFieldServerPort.text;
    }
}

- (void)testConnect {
	// No failures yet.
	//XCTAssertNil([client lastError]);
	
	// Nothing is listening yet.
	//XCTAssertFalse([client connect]);
	//XCTAssertNotNil([client lastError]);
	
	// Spawn a thread to listen.
	[NSThread detachNewThreadSelector:@selector(simpleListen:) toTarget:self withObject:nil];
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Connection should now succeed.
	//XCTAssertTrue([client connect]);
	//STAssertNil([client lastError], @"Last error should be nil"); // TODO: Not sure if this should be cleared out or just left alone.
}

- (void)testConnectWithDefaultTimeout {
	// Connect to a non-routable IP address. See http://stackoverflow.com/a/904609/209371
	[client close];
	client = [[FastSocket alloc] initWithHost:@"10.255.255.1" andPort:@"81"];
	
	// Connection should timeout.
	NSTimeInterval startTime = [NSDate timeIntervalSinceReferenceDate];
	//XCTAssertFalse([client connect]);
	NSTimeInterval endTime = [NSDate timeIntervalSinceReferenceDate];
	//XCTAssertNotNil([client lastError]);
	
	// Default timeout is 75 seconds on my machine.
	NSTimeInterval actualTime = endTime - startTime;
	//XCTAssertTrue(actualTime >= 75.0, @"timeout was %.2f", actualTime);
	//XCTAssertTrue(actualTime < 76.0, @"timeout was %.2f", actualTime);
}

- (void)testConnectWithCustomTimeout {
	// Connect to a non-routable IP address. See http://stackoverflow.com/a/904609/209371
	[client close];
	client = [[FastSocket alloc] initWithHost:@"10.255.255.1" andPort:@"81"];
	
	// Connection should timeout.
	NSTimeInterval startTime = [NSDate timeIntervalSinceReferenceDate];
	//XCTAssertFalse([client connect:10]);
	NSTimeInterval endTime = [NSDate timeIntervalSinceReferenceDate];
	//XCTAssertNotNil([client lastError]);
	
	// Check the duration of the timeout.
	NSTimeInterval actualTime = endTime - startTime;
	//XCTAssertTrue(actualTime >= 9.9, @"timeout was %.2f", actualTime);
	//XCTAssertTrue(actualTime < 10.1, @"timeout was %.2f", actualTime);
}

- (void)testIsConnected {
	// Spawn a thread to listen.
	[NSThread detachNewThreadSelector:@selector(simpleListen:) toTarget:self withObject:nil];
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Check connected state before and after connecting, and after closing.
	//XCTAssertFalse([client isConnected]);
	//XCTAssertTrue([client connect]);
	//XCTAssertTrue([client isConnected]);
	//XCTAssertTrue([client close]);
	//XCTAssertFalse([client isConnected]);
	
	// Close server socket and verify that the client knows it's no longer connected.
	//XCTAssertTrue([client connect]);
	//XCTAssertTrue([client isConnected]);
	//XCTAssertTrue([server close]);
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.2]];
	//XCTAssertFalse([client isConnected]);
}

- (void)testTimeoutBefore {
	// Spawn a thread to listen.
	[NSThread detachNewThreadSelector:@selector(simpleListen:) toTarget:self withObject:nil];
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Set value before connect.
	//XCTAssertTrue([client setTimeout:100]);
	//XCTAssertTrue([client connect]);
	//XCTAssertEqual([client timeout], 100L);
	//XCTAssertTrue([client close]);
}

- (void)testTimeoutAfter {
	// Spawn a thread to listen.
	[NSThread detachNewThreadSelector:@selector(simpleListen:) toTarget:self withObject:nil];
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Set value after connect.
	//XCTAssertTrue([client connect]);
	//XCTAssertTrue([client setTimeout:100]);
	//XCTAssertEqual([client timeout], 100L);
	//XCTAssertTrue([client close]);
}

- (void)testTimeoutMultipleBefore {
	// Spawn a thread to listen.
	[NSThread detachNewThreadSelector:@selector(simpleListen:) toTarget:self withObject:nil];
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Set value twice before connect.
	//XCTAssertTrue([client setTimeout:100]);
	//XCTAssertTrue([client setTimeout:101]);
	//XCTAssertTrue([client connect]);
	//XCTAssertEqual([client timeout], 101L);
	//XCTAssertTrue([client close]);
}

- (void)testTimeoutMultipleAfter {
	// Spawn a thread to listen.
	[NSThread detachNewThreadSelector:@selector(simpleListen:) toTarget:self withObject:nil];
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Set value twice after connect.
	//XCTAssertTrue([client connect]);
	//XCTAssertTrue([client setTimeout:100]);
	//XCTAssertTrue([client setTimeout:101]);
	//XCTAssertEqual([client timeout], 101L);
	//XCTAssertTrue([client close]);
}

- (void)testSegmentSizeBefore {
	// Spawn a thread to listen.
	[NSThread detachNewThreadSelector:@selector(simpleListen:) toTarget:self withObject:nil];
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Set value before connect.
	//XCTAssertTrue([client setSegmentSize:10000]);
	//XCTAssertTrue([client connect]);
	//XCTAssertEqual([client segmentSize], 10000);
	//XCTAssertTrue([client close]);
}

- (void)testSegmentSizeAfter {
	// Spawn a thread to listen.
	[NSThread detachNewThreadSelector:@selector(simpleListen:) toTarget:self withObject:nil];
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Set value after connect.
	//XCTAssertTrue([client connect]);
	//XCTAssertTrue([client setSegmentSize:10000]);
	//XCTAssertEqual([client segmentSize], 10000);
	//XCTAssertTrue([client close]);
}

- (void)testSegmentSizeMultipleBefore {
	// Spawn a thread to listen.
	[NSThread detachNewThreadSelector:@selector(simpleListen:) toTarget:self withObject:nil];
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Set value twice before connect.
	//XCTAssertTrue([client setSegmentSize:10000]);
	//XCTAssertTrue([client setSegmentSize:10001]);
	//XCTAssertTrue([client connect]);
	//XCTAssertEqual([client segmentSize], 10001);
	//XCTAssertTrue([client close]);
}

- (void)testSegmentSizeMultipleAfter {
	// Spawn a thread to listen.
	[NSThread detachNewThreadSelector:@selector(simpleListen:) toTarget:self withObject:nil];
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Set value twice after connect.
	//XCTAssertTrue([client connect]);
	//XCTAssertTrue([client setSegmentSize:10000]);
	//XCTAssertFalse([client setSegmentSize:10001]);
	//XCTAssertTrue([client setSegmentSize:9999]);
	//XCTAssertEqual([client segmentSize], 9999);
	//XCTAssertTrue([client close]);
}

- (void)testSendingAndReceivingBytes {
	// Spawn a thread to listen.
	[NSThread detachNewThreadSelector:@selector(listenAndRepeat:) toTarget:self withObject:nil];
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Send a byte array.
	long len = 10;
	unsigned char sent[] = {1, -2, 3, -4, 5, -6, 7, -8, 9, 0};
	[client connect];
	long count = [client sendBytes:sent count:len];
	//XCTAssertEqual(count, len, @"send error: %@", [client lastError]);
	[[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Receive a byte array.
	unsigned char received[len];
	count = [client receiveBytes:received limit:len];
	//XCTAssertEqual(count, len, @"receive error: %@", [client lastError]);
	
	// Compare results.
	//XCTAssertEqual(memcmp(sent, received, len), 0);
	[client close];
}

- (void)testSendingAndReceivingRandomBytes {
	// Spawn a thread to listen.
	[NSThread detachNewThreadSelector:@selector(listenAndRepeat:) toTarget:self withObject:nil];
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Create a random byte array larger than the buffer.
	long len = 1024 * 10 * 200;
	unsigned char sent[len];
	for (int i = 0; i < len; ++i) {
		sent[i] = (unsigned char)(random() % 256);
	}
	NSLog(@"sending %li bytes", len);
	
	// Send the array.
	[client connect];
	long count = [client sendBytes:sent count:len];
	//XCTAssertEqual(count, len, @"send error: %@", [client lastError]);
	[[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Receive the array.
	unsigned char received[len];
	//XCTAssertTrue([client receiveBytes:received count:len], @"receive error: %@", [client lastError]);
	
	// Compare results.
	//XCTAssertEqual(memcmp(sent, received, len), 0);
	[client close];
}
/*
- (void)testSendingAndReceivingStrings {
	// Spawn a thread to listen.
	[NSThread detachNewThreadSelector:@selector(listenAndRepeat:) toTarget:self withObject:nil];
	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	[client connect];
	
	// Send the string.
	NSString *original = @"This is å striñg to tëst séndîng. 😎";
	NSData *data = [original dataUsingEncoding:NSUTF8StringEncoding];
	long len = [data length];
	long count = [client sendBytes:[data bytes] count:len];
	//XCTAssertEqual(count, len, @"send error: %@", [client lastError]);
	[[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
	
	// Receive the string.
	char bytes[len];
	//XCTAssertTrue([client receiveBytes:bytes count:len], @"receive error: %@", [client lastError]);
	NSString *received = [[NSString alloc] initWithBytes:bytes length:len encoding:NSUTF8StringEncoding];
	
	// Compare results.
	//XCTAssertEqualObjects(received, original);
	[client close];
}
*/
- (void)testSendingAndReceivingStrings {
    
	// Spawn a thread to listen.
    
//	[NSThread detachNewThreadSelector:@selector(listenAndRepeat:) toTarget:self withObject:nil];
//	[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        
        // ここに非同期でバックグラウンドに投げたい処理を書く
        BOOL ret = YES;

        dispatch_async(dispatch_get_main_queue(), ^{
            
            NMLabelServerStatus.text = @"started listening";
            
        });
        
		NSLog(@"started listening");
		[server listen];
		
        
        while (ret){
            
            ret = [self listenAndRepeat:nil];
            
        }
        
    });

}

- (void)simpleListen:(id)obj {
	@autoreleasepool {
		[server listen]; // Incoming connections just queue up.
	}
}

- (BOOL)listenAndRepeat:(id)obj {
    
	@autoreleasepool {
        
		FastSocket *incoming = [server accept];
		if (!incoming) {
			NSLog(@"accept error: %@", [server lastError]);
		}
		
		// Read some bytes then echo them back.
        
		int bufSize = 2048;
		//unsigned char buf[bufSize];
        
		long count = 0;
		do {
			// Read bytes.
			count = [incoming receiveBytes:buf limit:bufSize];
			
			// Write bytes.
			long remaining = count;
			while (remaining > 0) {
				count = [incoming sendBytes:buf count:remaining];
				remaining -= count;
			}
			
			// Allow other threads to work.
			[[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
            
		} while (count > 0);
		
		NSLog(@"stopped listening with error: %@", (count < 0 ? [incoming lastError] : @"none"));
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NMUILabelRecievedMsg.text = [NSString stringWithFormat:@"%s" , buf];
            
        });
        
        AudioServicesPlaySystemSound(1000);
        
        return YES;
        
	}
    
    return NO;
}



@end
