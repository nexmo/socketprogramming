//
//  nmAppDelegate.h
//  socketProgramming
//
//  Created by sangjune moon on 2014. 12. 30..
//  Copyright (c) 2014년 nexmo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface nmAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
